package sh.calaba.demoproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button b = (Button)findViewById(R.id.button);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = "test";
                String pw = "123456";

                EditText user = (EditText) findViewById(R.id.userId);
                EditText pass = (EditText) findViewById(R.id.userPw);

                if(user.getText().toString().equals(id) && pass.getText().toString().equals(pw))
                {
                    setContentView(R.layout.logined);

                    Button b2 = (Button)findViewById(R.id.logoutBtn);

                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MyActivity ac = new MyActivity();

                            setContentView(R.layout.main);
                        }
                    });
                }
                else
                    new AlertDialog.Builder(getWindow().getContext()).setMessage("Login Failed").show();
            }
        });
    }
}
